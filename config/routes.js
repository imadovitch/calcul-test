/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  'GET /bonjour': 'BonjourController.bonjour',
  'GET /calculatrice': {
    view: 'pages/calculatrice/calculatrice',
    locals: {
      layout: null
    }
  },



  'POST /calculatrice/additionner': 'CalculatriceController.additionner',


  'POST /calculatrice/soustraire': 'CalculatriceController.sous',


  'POST /calculatrice/diviser': 'CalculatriceController.divi',


 // 'POST /calculatrice/multiplier': 'CalculatriceController.multiplier',


};
