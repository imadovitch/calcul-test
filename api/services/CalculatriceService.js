module.exports = {

    additionner: function (a, b) {
        return a + b;
    },

    //arrow functions
    soustraire: (a, b) => { return a - b },

    mult: function (a, b) {
        return a * b;
    },

    //=== -> !==
    divi: function (a, b) {
        if (b !== 0) {
            return a / b;
        } else {
            throw new Error("div 0");
        }
        //infinity
        //NaN : comme nombre .. peut additionner
        //=== type+valeur
    },

    percent: function (percents, from) {
        return this.mult(this.divi(percents, 100), from);
        //infinity
        //NaN : comme nombre .. peut additionner
        //=== type+valeur
    },

    percentMockable: function (percents, from) {
        return this.divi(percents, 100) * from;
        //infinity
        //NaN : comme nombre .. peut additionner
        //=== type+valeur
    },

    pow: function (a, b) {
        res = 1;
        for (let cpt = 1; cpt <= b; cpt++) {
            res = this.mult(res, b);
        }
        return res;
    },
}