const CalculatriceService = require("../services/CalculatriceService");

module.exports = {
 additionner:function(req,res){
    console.log(req.body);
    let val1= Number(req.body.val1);
    let val2= Number(req.body.val2);
    let resultat=CalculatriceService.additionner(val1,val2);
    res.view("pages/calculatrice/calculatrice",{result:resultat,layout:null}
    );

 },
 divi:function(req,res){
    console.log(req.body);
    let val1= Number(req.body.val1);
    let val2= Number(req.body.val2);
    let resultat=CalculatriceService.divi(val1,val2);
    res.view("pages/calculatrice/calculatrice",{result:resultat,layout:null}
    );
 },
 sous:function(req,res){
    console.log(req.body);
    let val1= Number(req.body.val1);
    let val2= Number(req.body.val2);
    let resultat=CalculatriceService.soustraire(val1,val2);
    res.view("pages/calculatrice/calculatrice",{result:resultat,layout:null}
    );
 }
};
