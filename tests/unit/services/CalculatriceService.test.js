
const assert = require('assert');
var sinon = require("sinon");


describe('CalculatriceService', function () {
  //nominal
  describe('#additionner()', function () {
    it('should return a+b', function (done) {
      let a = 5;
      let b = 3;
      let res = CalculatriceService.additionner(a, b);
      assert.equal(res, 8);
      return done();
    });
    //exception
    it('should return a+b neg', function (done) {
      let a = -5;
      let b = -3;
      let res = CalculatriceService.additionner(a, b);
      assert.equal(res, -8);
      return done();
    });
  });



  /*   it('should return a-b', function (done) {
      let a = 5;
      let b = 3;
      let res = CalculatriceService.soustraire(a, b);
      assert.equal(res, 2);
      return done();
    }); */









  describe('#soustraire()', function () {
    //nominal
    it('should return a-b', function (done) {
      let a = 5;
      let b = 3;
      let res = CalculatriceService.soustraire(a, b);
      assert.equal(res, 2);
      return done();
    });
    //exception 0
    it('should return a-b', function (done) {
      let a = -5;
      let b = -3;
      let res = CalculatriceService.soustraire(a, b);
      assert.equal(res, -2);
      return done();
    });


  });



  describe('#mult()', function () {
    //nominal
    it('should return a*b', function (done) {
      let a = 5;
      let b = 3;
      let res = CalculatriceService.mult(a, b);
      assert.equal(res, 15);
      return done();
    });



  });




  describe('#divi()', function () {
    //nominal
    it('should return a/b', function (done) {
      let a = 10;
      let b = 5;
      let res = CalculatriceService.divi(a, b);
      assert.equal(res, 2);
      return done();
    });

    //special
    it('should throw Error', function (done) {
      let a = 10;
      let b = 0;
       
      assert.throws(() => { CalculatriceService.divi(a, b) }, { message: 'div 0' });
      return done();
    });

  });






  describe('#percent()', function () {
    //nominal
    it('proxy mock pure unit', function (done) {
      //var callback = sinon.fake();
      //var proxy = once(callback);

      let a = 15;
      let b = 200;


      //let resDivi = CalculatriceService.divi(aa, bb);
      let resPercent = CalculatriceService.percentMockable(a, b);

      //https://sinonjs.org/releases/v15/mocks/
      //var mock = sinon.mock(resPercent);
      //var expectation = mock.expects("percent");



      //var stub = 
      sinon.stub(CalculatriceService, 'divi').callsFake((a, b) => { return 15 })
      assert.equal(resPercent, 30);
      return done();
    });






  });



  /* describe('#divi()', function () {
    //nominal
    it('should return a/b', function (done) {
      let a = 5;
      let b = 3;
      let res = CalculatriceService.additionner(a, b);
      assert.equal(res, 8);
      return done();
    });
    //exception 0
    it('should return a/b', function (done) {
      let a = -5;
      let b = 0;
      let res = CalculatriceService.additionner(a, b);
      assert.equal(res, -8);
      return done();   
    });

  }); */





  describe('#pow()', function () {
    //nominal
    it('normal run', function (done) {
      //var callback = sinon.fake();
      //var proxy = once(callback);

      let a = 3;
      let b = 3;


      //let resDivi = CalculatriceService.divi(aa, bb);
      let resPercent = CalculatriceService.pow(a, b);
      console.log('console = ' + resPercent);
      //https://sinonjs.org/releases/v15/mocks/
      //var mock = sinon.mock(resPercent);
      //var expectation = mock.expects("percent");



      //var stub = 
      //) horner idemS 

      //sinon.stub(CalculatriceService, 'mult').callsFake((a, b) => { return 15 })
      assert(true);
      return done();
    });
  });





  describe('#pow()', function () {
    //nominal
    it('pow mock', function (done) {
      //var callback = sinon.fake();
      //var proxy = once(callback);

      let a = 3;
      let b = 3;


      //let resDivi = CalculatriceService.divi(aa, bb);
      let resPercent = CalculatriceService.pow(a, b);

      //https://sinonjs.org/releases/v15/mocks/
      //var mock = sinon.mock(resPercent);
      //var expectation = mock.expects("percent");



      //var stub = 
      //) horner idemS 

      var stub = sinon.stub(CalculatriceService, 'mult').callsFake((a, b) => { return 15 })
      assert.equal(resPercent, 27);
      stub.restore();
      return done();
    });








    //nominal
    it('pow spy mock', function (done) {
      //var callback = sinon.fake();
      //var proxy = once(callback);

      let a = 3;
      let b = 3;


      //let resDivi = CalculatriceService.divi(aa, bb);

      //https://sinonjs.org/releases/v15/mocks/
      //var mock = sinon.mock(resPercent);
      //var expectation = mock.expects("percent");



      //var stub = 
      //) horner idemS 

      //var stubMul1 = sinon.stub(CalculatriceService, 'mult').callsFake((a, b) => { return 15 })
      //assert.equal(resPercent, 27);


      //https://semaphoreci.com/community/tutorials/best-practices-for-spies-stubs-and-mocks-in-sinon-js
      var spy = sinon.spy(CalculatriceService, 'mult');
      //setupNewUser({ name: 'test' }, function () { });

      let resPercent = CalculatriceService.pow(a, b);

      //sinon.assert.calledOnce(stubMul2);
      assert.equal(spy.callCount, 3);
      //stubMul1.restore();

      return done();
    });


  });
});

