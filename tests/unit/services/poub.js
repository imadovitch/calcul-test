
var util = require('util');

describe('CalculatriceService (model)', function() {

  describe('#findBestStudents()', function() {
    it('should return a+b', function (done) {
      additionner(1,2)
      .then(function(bestStudents) {

        if (bestStudents.length !== 5) {
          return done(new Error(
            'Should return exactly 5 students -- the students '+
            'from our test fixtures who are considered the "best".  '+
            'But instead, got: '+util.inspect(bestStudents, {depth:null})+''
          ));
        }//-•

        return done();

      })
      .catch(done);
    });
  });

});