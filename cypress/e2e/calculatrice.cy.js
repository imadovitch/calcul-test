describe('url', () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit('http://127.0.0.1:1337/calculatrice/');
  })

  /*https://docs.cypress.io/guides/core-concepts/introduction-to-cypress#Cypress-Can-Be-Simple-Sometimes*/
  it('passes', () => {
    cy.get('#id7').click({ force: true });
    cy.get('#idButtonPlus').click({ force: true });

    cy.get('#id4').click({ force: true });
    cy.get('#idEquals').click({ force: true });


    cy.get('#idAffich').should('have.value', '11')
  })

  it('passes', () => {
    cy.get('#id7').click({ force: true });
    cy.get('#idButtonPlus').click({ force: true });

    cy.get('#id4').click({ force: true });
    cy.get('#idEquals').click({ force: true });

    

    cy.get('#idAffich').should('have.value', '11')
  })
})